
import { Component } from 'react';
import './styles.scss';

class Header extends Component {
  render() {
    return (
      <div className="Header">
        React Demo: Emoji Search
      </div>
    );
  }
}

export default Header;
